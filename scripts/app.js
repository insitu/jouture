/*
 Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
 This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 Code distributed by Google as part of the polymer project is also
 subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */


(function(document) {
    'use strict';

    // Grab a reference to our auto-binding template
    // and give it some initial binding values
    // Learn more about auto-binding templates at http://goo.gl/Dx1u2g
    var app = document.querySelector('#app');

    //USER Session
    app.sessionExists = false;
    app.subheaderVisible = true;
    app.session = null; //document.querySelector('#session');
    app.dialogSpinner = document.querySelector('#dialogSpinner');
    app.confirmationDialog = document.querySelector('#confirmationDialog');
    app.headerHeight = 42; //69; //103;//143;
    app.subheaderHeight = 33; //69; //103;//143;
    app.badges = {};
    app.game = {};


    app.currentLevelCommunity = 1;
    app.currentLevelProfile = 1;

    app.apiphp = 'http://www.kulectiv.com:88';
    app.apinode = 'https://www.kulectiv.com:3000';
    // app.apinode = 'http://localhost:3000';
    // app.apinode = 'http://192.168.3.103:3000';

    // Versions
    app.mainAppVersion = 'v1.51';
    app.modulesMainVersion = 'v1.52';
    app.landingPageVersion = 'v1.32';
    app.signupMainVersion = 'v1.30';
    app.adminDashboardVersion = 'v1.12';
    // TODO: Tener en cuenta que dentro de index.html se importan los siguientes archivos:
    /* login-twitter.html in-session.html routing.html elements.html */

    app.validateTag = function(tagName) {
        // const letterNumber = /^[0-9a-zA-Z]+$/;
        // const letters = /^[A-Za-z]+$/;
        const lettersNumbersAndSpaces = /^[0-9a-zA-Z\s]*$/;
        if(tagName === '' || tagName.length < 1 || tagName.length > 45 || !tagName.match(lettersNumbersAndSpaces)){
            return false;
        }
        return true;
    }

    app.profilePictureMini = function (fileName) {
        if(fileName){
            return app.apinode + "/images/get.picture/mini/" + fileName + '?' + Math.random();
        }else{
            var pictureName = session.getPicture() ? session.getPicture() : 'default.png';
            return app.apinode + "/images/get.picture/mini/" + pictureName + '?' + Math.random();
        }
    }

    app.profilePictureThumbnail = function (fileName) {
        if(fileName){
            return app.apinode + "/images/get.picture/thumbnail/" + fileName + '?' + Math.random();
        }else{
            var pictureName = session.getPicture() ? session.getPicture() : 'default.png';
            return app.apinode + "/images/get.picture/thumbnail/" + pictureName + '?' + Math.random();
        }
    }

    app.socket = null;
    app.socketConnect = function(){

        if(app.socket){
            return;
        }

        //var options = {forceNew: true, reconnection: true, reconnectionDelayMax: 15000, reconnectionDelay:1000, query: "user="+app.session.getUser()};
        // var options = {forceNew: false, reconnection: false, query: "user="+app.session.getUser()};
        var options = {forceNew: true, reconnection: true, query: "user="+app.session.getUser()};
        app.socket = io.connect(app.apinode, options);

        app.socket.on('session-expired', function () {
            app.showToast('Session expired', 'session');
            app.logout();
        });

        app.socket.on('connect', function () {
            // console.log('server connected');
            var dialog = document.querySelector('#dialogOffline');
            dialog.close();

        });

        app.socket.on('connect_error', function (e) {
            // console.log('connect_error');
        });

        app.socket.on('disconnect', function () {
            // console.log('server disconnected');
            if(!app.logout){
                var dialog = document.querySelector('#dialogOffline');
                dialog.open();
            }
        });

        app.socket.on('reconnect', function (e) {
            // console.log('server reconnected');
        });

        app.socket.on('reconnect_error', function (e) {
            // console.log('reconnect_error');
            var dialog = document.querySelector('#dialogOffline');
            dialog.open();
        });

        app.socket.on('init-notifications', function (notificationCounters) {

            var expertNotification = document.querySelector('#expert');
            if(expertNotification){
                expertNotification.initCounters(notificationCounters);
            }else {
                document.addEventListener('expert-attached', function(){
                    var expertNotification = document.querySelector('#expert');
                    expertNotification.initCounters(notificationCounters);
                });
            }
        });

        app.socket.on('new-notification', function (notification) {
            var expertNotification = document.querySelector('#expert');
            expertNotification.updateCounters(notification);
        });

    };

    app.cloneObject = function(obj){

        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    };

    app.hideLandingHeader = function () {
        var landingheader = document.querySelector('#landingHeader');
        if(!landingheader){
            return;
        }
        landingheader.style.display = 'none';
    };

    app.showLandingHeader = function () {
        var landingheader = document.querySelector('#landingHeader');
        if(!landingheader){
            return;
        }
        landingheader.style.display = 'block';
    };

    app.hideDrawerPanel = function(){

        var mainContent = document.querySelector('#mainContent');
        mainContent.$.paperDrawerPanel.forceNarrow = true;
        mainContent.$.paperDrawerPanel.disableSwipe = true;

        var button = document.querySelector('#mobileButtonOpenDrawer');
        if(button){
            // button.style = 'display: none !important';
            button.style.display = 'none !important';
        }
    };

    app.showDrawerPanel = function(){
        var mainContent = document.querySelector('#mainContent');
        mainContent.$.paperDrawerPanel.disableSwipe = false;
        mainContent.$.paperDrawerPanel.forceNarrow = false;
        var button = document.querySelector('#mobileButtonOpenDrawer');
        if(button && app.mobile){
            button.style = 'display: flex';
        }
    };

    app.displayInstalledToast = function() {
        // Check to make sure caching is actually enabled—it won't be in the dev environment.
        if (!document.querySelector('platinum-sw-cache').disabled) {
            document.querySelector('#caching-complete').show();
        }
    };

    app.checkSession = function(){

        if(!app.session){
            app.session = document.querySelector('#session');
        }

        var user = app.session.getUser();

        if(!app.session || user === null || user === '' || user === undefined){
            return false;
        }
        return true;
    };


    app.addEventListener('dom-change', function() {

        // //Verificar si existe session activa
        app.session = document.querySelector('#session');

        // //Browser compatible con local storage
        // if(typeof Storage !== undefined){
        //
        //     app.session.addEventListener('iron-localstorage-load-empty', function(){
        //         //
        //     });
        //
        //     app.session.addEventListener('iron-localstorage-load', function(){
        //         app.socketConnect();
        //     });
        //
        // }else{
        //     app.socketConnect();
        // }

        if(navigator.onLine){
            var dialog = document.querySelector('#dialogOffline');
            dialog.close();
        }else{
            var dialog = document.querySelector('#dialogOffline');
            dialog.open();
        }

        window.addEventListener('online',  function(){
            var dialog = document.querySelector('#dialogOffline');
            dialog.close();
        });
        window.addEventListener('offline', function(){
            var dialog = document.querySelector('#dialogOffline');
            dialog.open();
        });


        document.querySelector('#apiGoogleMapsGlobal').addEventListener('api-load', function(){
            app.googleApiLoaded = true;
        }.bind(this));


        /* Cerrar ventana de notificaciones en caso de estar abierta */
        window.addEventListener('tap', function() {

            var disNoti = document.querySelector('#displayNotifications');

            if(disNoti && disNoti.opened){
                disNoti.toggle();
                disNoti.style.display = "none";
            }
        });

        var buttonContiner = document.querySelector('.index-buttons-container');

        if(buttonContiner){
            var buttonContinerHeight = window.innerHeight / 2 - 81 ;
            buttonContiner.style.marginTop = buttonContinerHeight + 'px';
        }

        /*Variable que se pone en true cuando la resolución de pantalla tiene menos de
         * 765px de ancho*/
        app.mobile = window.matchMedia("screen and (max-width: 765px)").matches;

        window.addEventListener('resize', function (e) {
            app.updateMainscrollHeight();
        }.bind(this));

    });


    // See https://github.com/Polymer/polymer/issues/1381
    window.addEventListener('WebComponentsReady', function() {
        // imports are loaded and elements have been registered
    });

    // Main area's paper-scroll-header-panel custom condensing transformation of
    // the appName in the middle-container and the bottom title in the bottom-container.
    // The appName is moved to top and shrunk on condensing. The bottom sub title
    // is shrunk to nothing on condensing.
    //addEventListener('paper-header-transform', function(e) {
    //  var appName = document.querySelector('#mainToolbar .app-name');
    //  var middleContainer = document.querySelector('#mainToolbar .middle-container');
    //  var bottomContainer = document.querySelector('#mainToolbar .bottom-container');
    //  var detail = e.detail;
    //  var heightDiff = detail.height - detail.condensedHeight;
    //  var yRatio = Math.min(1, detail.y / heightDiff);
    //  var maxMiddleScale = 0.50;  // appName max size when condensed. The smaller the number the smaller the condensed size.
    //  var scaleMiddle = Math.max(maxMiddleScale, (heightDiff - detail.y) / (heightDiff / (1-maxMiddleScale))  + maxMiddleScale);
    //  var scaleBottom = 1 - yRatio;
    //
    //  // Move/translate middleContainer
    //  Polymer.Base.transform('translate3d(0,' + yRatio * 100 + '%,0)', middleContainer);
    //
    //  // Scale bottomContainer and bottom sub title to nothing and back
    //  Polymer.Base.transform('scale(' + scaleBottom + ') translateZ(0)', bottomContainer);
    //
    //  // Scale middleContainer appName
    //  Polymer.Base.transform('scale(' + scaleMiddle + ') translateZ(0)', appName);
    //});

    // Close drawer after menu item is selected if drawerPanel is narrow
    app.onDataRouteClick = function() {
        var drawerPanel = document.querySelector('#paperDrawerPanel');
        if (drawerPanel.narrow) {
            drawerPanel.closeDrawer();
        }
    };

    /*
     *  This is called when user taps any where inside the app (main-app element).
     */
    app.onContentTap = function() {
        var header = this.$.mainHeader;
        header.hideSearch();
    };

    app.showToastPermanent = function (message, buttonLabel){
        this.$.inToast.showPermanent(message, buttonLabel);
    };

    app.showToast = function (message, type){

        if(!message){
            return;
        }

        this.$.inToast.close();

        switch(type){
            case 'success':
                this.$.inToast.setSuccessMode();
                break;

            case 'help':
                this.$.inToast.setGotItMode();
                break;

            case 'session':
                this.$.inToast.setGotItMode();
                break;

            case 'wonPoints':
                this.$.inToast.setRedirectionMode("Go to Dashboard", "dashboard");
                break;

            case 'newLevel':
                this.$.inToast.setRedirectionMode("Go to Dashboard", "dashboard");
                break;

            case 'collaborate':
                this.$.inToast.setRedirectionMode("Go to Collaborate", "connect");
                break;

            default:
                this.$.inToast.setRegularMode();
        }

        this.$.inToast.text = message;
        this.$.inToast.show();
    };

    app.logout = function(){

        dialogSpinner.open();

        this.$.ajaxLogout.url = app.apinode + "/login/logout"
        this.$.ajaxLogout.generateRequest();

        setTimeout(function () {
            if(app.socket){
                app.logout = true;
                app.socket.disconnect();
            }
            app.session.delete();
            //Reload and clear cache
            location.reload(true);
        }, 500);
    };

    /* NO USAR */
    app.formatDate = function(timestamp, isSeconds){
        // Create a new JavaScript Date object based on the timestamp
        // multiplied by 1000 so that the argument is in milliseconds, not seconds.
        var date = new Date(timestamp*1000);
        // Hours part from the timestamp
        var hours = date.getHours();
        // Minutes part from the timestamp
        var minutes = '0' + date.getMinutes();
        // Seconds part from the timestamp
        var seconds = '0' + date.getSeconds();

        // Will display time in 10:30:23 format
        return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

    };

    /* Usando actualmente */
    app.timeConverter = function (unixTimestamp, format){

        var a = new Date(unixTimestamp * 1000);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var fullMonths = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var fullMonth = fullMonths[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();

        var suf = 'th';

        if(parseInt(date) === 1){
            suf = 'st';
        }
        if(parseInt(date) === 2){
            suf = 'nd';
        }

        //var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;

        var stringResult;

        if(format === 'short'){
            stringResult = fullMonth + ' ' + date + ' ' + suf;
        }
        if(format === 'long'){
            stringResult = month + ' ' + date + ' ' + suf + ' ' + hour + ':' + min + ':' + sec ;
        }
        if(format === 'time'){
            var wm = hour > 12 ? "PM" : "AM";
            if(hour > 12){
                hour-=12;
            }
            stringResult = hour + ':' + ('0' +min).substr(-2) + ' ' + wm;
        }
        if(format === 'date'){
            stringResult = month + ' ' + date + ' ' + suf + ' ' + year ;
        }
        return stringResult;
    };

    app.timeAgo = function(oldTimestamp){

        var seconds = (Math.floor((new Date() / 1000))) - Math.floor(oldTimestamp / 1000);

        if(seconds < 60)
            return "now";
        if(seconds < 3600)
            return Math.floor(seconds / 60) + " minutes ago";
        if(seconds < 86400)
            return Math.floor(seconds / 3600) + " hours ago";
        else
            return this.timeConverter(oldTimestamp/1000, 'short');
    };


    // extracts time (msecs) from v1 type uuid
    app.timestampFromTimeuuid = function (buf, offset) {

        var msec = 0, nsec = 0;
        var i = buf && offset || 0;
        var b = buf||[];

        // inspect version at offset 6
        if ((b[i+6]&0x10)!=0x10) {
            throw new Error("uuid version 1 expected"); }

        // 'time_low'
        var tl = 0;
        tl |= ( b[i++] & 0xff ) << 24;
        tl |= ( b[i++] & 0xff ) << 16;
        tl |= ( b[i++] & 0xff ) << 8;
        tl |=   b[i++] & 0xff ;

        // `time_mid`
        var tmh = 0;
        tmh |= ( b[i++] & 0xff ) << 8;
        tmh |=   b[i++] & 0xff;

        // `time_high_minus_version`
        tmh |= ( b[i++] & 0xf ) << 24;
        tmh |= ( b[i++] & 0xff ) << 16;

        // account for the sign bit
        msec = 1.0 * ( ( tl >>> 1 ) * 2 + ( ( tl & 0x7fffffff ) % 2 ) ) / 10000.0;
        msec += 1.0 * ( ( tmh >>> 1 ) * 2 + ( ( tmh & 0x7fffffff ) % 2 ) ) * 0x100000000 / 10000.0;

        // Per 4.1.4 - Convert from Gregorian epoch to unix epoch
        msec -= 12219292800000;

        // getting the nsec. they are not needed now though
        // nsec = ( tl & 0xfffffff ) % 10000;

        return msec;
    };


    app.handleAjaxResponse = function(e){
        //
    };

    app.ajaxResponse = function(e){

        if(!e.detail.response)
            return true;

        // if(e.detail.response.status != '200'){
        //     this.showToast(e.detail.response.message);
        // }

        this.handleAjaxResponse(e);
    };


    app.hasClass = function (el, className) {
        if (el.classList)
            return el.classList.contains(className)
        else
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
    };

    app.addClass = function (el, className) {
        if (el.classList)
            el.classList.add(className)
        else if (!hasClass(el, className)) el.className += " " + className
    };

    app.removeClass = function (el, className) {
        if (el.classList)
            el.classList.remove(className)
        else if (hasClass(el, className)) {
            var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
            el.className=el.className.replace(reg, ' ')
        }
    };

    app.getHeight = function () {
        var myWidth = 0, myHeight = 0;
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            myHeight = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            myHeight = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            myHeight = document.body.clientHeight;
        }
        return myHeight;
    };

    app.fadein = function(element) {
        var op = 0.1;  // initial opacity
        element.style.display = 'block';
        var timer = setInterval(function () {
            if (op >= 1){
                clearInterval(timer);
            }
            element.style.opacity = op;
            element.style.filter = 'alpha(opacity=' + op * 100 + ")";
            op += op * 0.1;
        }, 20);
    };

    app.fadeout = function(element) {
        var op = 1;  // initial opacity
        var timer = setInterval(function () {
            if (op <= 0.1){
                clearInterval(timer);
                element.style.display = 'none';
            }
            element.style.opacity = op;
            element.style.filter = 'alpha(opacity=' + op * 100 + ")";
            op -= op * 0.1;
        }, 50);
    };

    app.stopPropagation = function(e){
        e.stopPropagation();
        e.preventDefault();
        if (e.gesture)
            e.gesture.stopPropagation();
        if (e.gesture)
            e.gesture.preventDefault();
    };

    app.logActivity = function(type, objects_id, details, usersData){

        app.controlGameStatus(type);

        objects_id      || ( objects_id = [] );
        details         || ( details = [] );
        usersData       || ( usersData  = {} );

        var users_id =      usersData.users_id || [];
        var users_name =    usersData.users_name || [];
        var users_pic =     usersData.users_pic || [];

        // It can receive a value or an array of values
        if(objects_id.constructor != Array){
            objects_id = objects_id + ''; // convertir a string
            objects_id = [objects_id];
        }else{
            for(var i=0; i<objects_id.length; i++) objects_id[i] = objects_id[i] + ''; // convertir a string
        }
        if(details.constructor != Array){
            details = [details];
        }
        if(users_id.constructor != Array){
            users_id = [users_id];
        }
        if(users_name.constructor != Array){
            users_name = [users_name];
        }
        if(users_pic.constructor != Array){
            users_pic = [users_pic];
        }

        var currentUserID = session.getUser();
        var currentUserName = session.getName();
        var currentUserPicture = session.getPicture();

        var params = {
            type:               type,
            user_id:            currentUserID,
            user_name:          currentUserName,
            user_pic:           currentUserPicture,
            target_object_id:   objects_id[0] ? objects_id[0] : null,
            objects_id:         objects_id,
            details:            details,
            users_id:           users_id,
            users_name:         users_name,
            users_pic:          users_pic
        };

        if(app.socket){
            app.socket.emit('log-activity', JSON.stringify(params));
            return false;
        }

        // Devolvemos parametros para poder hacer llamada ajax.
        return params;
    };

    //Recive el tipo de activity y notifica visualmente
    app.controlGameStatus = function(type){

        if(!app.game.items || !app.game.activities || type === app.constants.USER_LOGIN){
            return;
        }

        if( type == app.constants.DISCUSSION_UNPIN ){
            for( var e = 0; e < app.game.activities.length; e++ ){
                if(app.game.activities[e].type == app.constants.DISCUSSION_PIN){
                    app.game.activities[e].counter--;
                    break;
                }
            }
        }else if( type == app.constants.USER_UNFOLLOW ){
            for( var e = 0; e < app.game.activities.length; e++ ){
                if(app.game.activities[e].type == app.constants.USER_FOLLOW){
                    app.game.activities[e].counter--;
                    break;
                }
            }
        }


        //Establecer nivel actual
        var levels = app._getGamificationLeves();
        var communityLevel = levels.community;
        var profileLevel = levels.profile;

        //Revisar que la acción sea una tarea pendiente de las tareas del nivel actual
        for( var i = 0; i < app.game.items.length; i++ ){

            // Si el tipo de activity es USER_ENGAGE_REQUEST cambiar el tipo a USER_ENGAGE_REQUEST_SENDER (es el tipo usado en el counter)
            var comparableType = type == "USER_ENGAGE_REQUEST" ? "USER_ENGAGE_REQUEST_SENDER" : type;

            if(app.game.items[i].completed || app.game.items[i].type != comparableType){
                continue;
            }

            if((app.game.items[i].game === "community" && app.game.items[i].level === communityLevel)
                || (app.game.items[i].game === "profile" && app.game.items[i].level === profileLevel)){

                this._checkQuantityGoal(comparableType, app.game.items[i].level, app.game.items[i].game);
            }

            break;
        }

    };

    app._getGamificationLeves =  function(){

        //Identificar los niveles actuales
        var communityLevel = 0;
        var profileLevel = 0;

        var communityIncompleteFound = false;
        var profileIncompleteFound = false;

        if (!app.game.summaries) {
            return {community: 1, profile: 1};
        }

        for(var i = 0; i < app.game.summaries.length; i++){

            if(app.game.summaries[i].game === "community" && !communityIncompleteFound){
                if(app.game.summaries[i].done){
                    communityLevel = communityLevel < app.game.summaries[i].level ? app.game.summaries[i].level : communityLevel;
                }else{
                    communityIncompleteFound = true;
                }
            }

            if( app.game.summaries[i].game === "profile" && !profileIncompleteFound ){
                if(app.game.summaries[i].done){
                    profileLevel = profileLevel < app.game.summaries[i].level ? app.game.summaries[i].level : profileLevel;
                }else{
                    profileIncompleteFound = true;
                }
            }

        }

        return {community: (communityLevel+1), profile: (profileLevel+1)};
    };

    app._checkQuantityGoal = function(type, level, gameName){

        var activityCounter = 0;
        var quantityGoal = 0;

        var activityExist = false;

        //Incrementar el activity counter en uno
        for( var i = 0; i < app.game.activities.length; i++ ){
            if( app.game.activities[i].type === type ){
                activityExist = true;
                activityCounter = ++app.game.activities[i].counter;
                break;
            }
        }
        if(!activityExist){
            app.game.activities.push({type: type, counter: 1});
            activityCounter = 1;
        }

        //Calcular los Quantity Goal para "level"
        //Obtener el indice del item para el nivel actual para poder setear a "completed" si fuera necesario
        var currentItemIndex = 0;

        for( var i = 0; i < app.game.items.length; i++ ){

            if( app.game.items[i].type === type && app.game.items[i].level <= level ){

                //Guardar el indice de la tarea actual para marcarla como completa en caso de corresponder
                if(level == app.game.items[i].level){
                    currentItemIndex = i;
                }

                //Sumarizar el total de repeticiones de la tarea en todos sus niveles hasta el actual
                quantityGoal += app.game.items[i].quantity_goal;
            }
        }

        //Si completo el quantityGoal del item pone el done en true
        if( activityCounter >= quantityGoal ){

            app.game.items[currentItemIndex].completed = true;

            // if (app.game.items[currentItemIndex].type !== 'PROFILE_ADD_REQUEST' &&
            //     app.game.items[currentItemIndex].type !== 'PROFILE_COMPLETE_LOCATION' &&
            //     app.game.items[currentItemIndex].type !== 'PROFILE_UPLOAD_PIC')
            // {
            if (app.game.items[currentItemIndex].game != 'profile' && app.game.items[currentItemIndex].level != 1) {
                setTimeout(function(){
                    var totalPoints = parseFloat(app.game.items[currentItemIndex].points) * app.game.items[currentItemIndex].quantity_goal;
                    app.showToast("You won "+ totalPoints +" points!", "wonPoints");
                }.bind(this), 1000);
            }

            //Incrementar completed tasks, verificar si el summary esta completo y setear summary.done = true
            for( var i = 0; i < app.game.summaries.length; i++ ){

                if(app.game.summaries[i].game == app.game.items[currentItemIndex].game
                    && app.game.summaries[i].level ==  app.game.items[currentItemIndex].level){

                    //Incrementar la "CompletedTasks" y si es igual a "TotalTasks" setear done en "true"
                    app.game.summaries[i].completedTasks++;

                    if(app.game.summaries[i].completedTasks == app.game.summaries[i].totalTasks){

                        app.game.summaries[i].done = true;

                        // If it's the first level complete, GO to collaborate
                        if(level + 1 == 2 && gameName == 'profile'){

                            // setTimeout(function(){
                                // app.showToast("Awesome! You reached the level " + (level + 1) +" and activated the Collaborate stack. ","help");
                                // app.showToast("Awesome! you activated the Collaborate stack. ","collaborate");
                                // app.showToastPermanent("Awesome! you activated the Home page. ");
                                // page("/connect");
                            // }.bind(this),5500);

                            this.$.ajaxNoResponse.url = app.apinode + '/journalists/save.profile.complete';
                            this.$.ajaxNoResponse.generateRequest();

                        }else {
                            setTimeout(function(){
                                app.showToast("Congratulations! You reached the level " + (level + 1) +".","newLevel");
                            }.bind(this),6000);
                        }

                    }
                    break;
                }
            }
        }

    };


    app.constants = {

        NOTIFICATIONS_GLOBAL: 'NOTIFICATIONS_GLOBAL',

        CTA_TOOLBAR_HOME:        'CTA_TOOLBAR_HOME',
        CTA_TOOLBAR_DISCUSS:     'CTA_TOOLBAR_DISCUSS',
        CTA_TOOLBAR_COLLABORATE: 'CTA_TOOLBAR_COLLABORATE',
        CTA_TOOLBAR_RESOURCES:   'CTA_TOOLBAR_RESOURCES',
        CTA_TOOLBAR_GEAR:        'CTA_TOOLBAR_GEAR',
        CTA_TOOLBAR_LOGO:        'CTA_TOOLBAR_LOGO',
        CTA_TOOLBAR_PROJECT:     'CTA_TOOLBAR_PROJECT',


        CTA_TOOLBAR_PICTURE:       'CTA_TOOLBAR_PICTURE',
        CTA_TOOLBAR_NOTIFICATIONS: 'CTA_TOOLBAR_NOTIFICATIONS',
        CTA_TOOLBAR_MESSAGES:      'CTA_TOOLBAR_MESSAGES',
        CTA_TOOLBAR_SETTINGS:      'CTA_TOOLBAR_SETTINGS',

        CTA_FAB_DASHBOARD:               'CTA_FAB_DASHBOARD',
        CTA_HOME_POST_DISCUSSION:        'CTA_HOME_POST_DISCUSSION',
        CTA_HOME_GOTO_COLLABORATE:       'CTA_HOME_GOTO_COLLABORATE',
        CTA_HOME_POST_GEAR:              'CTA_HOME_POST_GEAR',
        CTA_RECOMMENDED_PROFILE_PICTURE: 'CTA_RECOMMENDED_PROFILE_PICTURE',
        CTA_RECOMMENDED_PROFILE_BUTTON:  'CTA_RECOMMENDED_PROFILE_BUTTON',

        CTA_GAMMIFICATION_HOME:  'CTA_GAMMIFICATION_HOME',

        PROJECT_POST: 'PROJECT_POST',
        PROJECT_POST_REPLY: 'PROJECT_POST_REPLY',
        PROJECT_ADD_MEMBER: 'PROJECT_ADD_MEMBER',

        DISCUSSION_POST: 'DISCUSSION_POST',
        DISCUSSION_POST_UP: 'DISCUSSION_POST_UP',
        DISCUSSION_POST_DOWN: 'DISCUSSION_POST_DOWN',
        DISCUSSION_POST_REPLY: 'DISCUSSION_POST_REPLY', //*****nueva
        DISCUSSION_REPLY: 'DISCUSSION_REPLY',
        DISCUSSION_REPLY_UP: 'DISCUSSION_REPLY_UP',
        DISCUSSION_REPLY_DOWN: 'DISCUSSION_REPLY_DOWN',
        DISCUSSION_PIN: 'DISCUSSION_PIN',
        DISCUSSION_UNPIN: 'DISCUSSION_UNPIN',
        DISCUSSION_READ: 'DISCUSSION_READ',

        USER_ENGAGE_REQUEST: 'USER_ENGAGE_REQUEST',
        USER_ENGAGE_REQUEST_SENDER: 'USER_ENGAGE_REQUEST_SENDER', //counter only
        USER_ENGAGE_REQUEST_TARGET: 'USER_ENGAGE_REQUEST_TARGET', //counter only
        USER_ENGAGE_ACCEPT: 'USER_ENGAGE_ACCEPT',
        USER_ENGAGE_ACCEPT_SENDER: 'USER_ENGAGE_ACCEPT_SENDER',   //counter only
        USER_ENGAGE_ACCEPT_TARGET: 'USER_ENGAGE_ACCEPT_TARGET',   //counter only - this is engaments counter

        USER_REFER: 'USER_REFER',

        USER_LOGIN: 'USER_LOGIN',
        USER_FOLLOW: 'USER_FOLLOW',
        USER_FOLLOWED: 'USER_FOLLOWED',
        USER_UNFOLLOW: 'USER_UNFOLLOW',

        PROFILE_UPLOAD_COVER: 'PROFILE_UPLOAD_COVER',
        PROFILE_UPLOAD_PIC: 'PROFILE_UPLOAD_PIC',
        PROFILE_COMPLETE_QUOTE: 'PROFILE_COMPLETE_QUOTE',
        PROFILE_COMPLETE_BIO: 'PROFILE_COMPLETE_BIO',
        PROFILE_COMPLETE_LOCATION: 'PROFILE_COMPLETE_LOCATION',
        PROFILE_COMPLETE_POSITION: 'PROFILE_COMPLETE_POSITION',
        PROFILE_ADD_PUBLICATION: 'PROFILE_ADD_PUBLICATION',
        PROFILE_ADD_LANGUAGE: 'PROFILE_ADD_LANGUAGE',
        PROFILE_DELETE_LANGUAGE: 'PROFILE_DELETE_LANGUAGE',
        PROFILE_ADD_EXPERTISE: 'PROFILE_ADD_EXPERTISE',
        PROFILE_DELETE_EXPERTISE: 'PROFILE_DELETE_EXPERTISE',
        PROFILE_ADD_OFFER: 'PROFILE_ADD_OFFER',
        PROFILE_DELETE_OFFER: 'PROFILE_DELETE_OFFER',
        PROFILE_ADD_REQUEST: 'PROFILE_ADD_REQUEST',
        PROFILE_DELETE_REQUEST: 'PROFILE_DELETE_REQUEST',
        PROFILE_UPDATE_TWITTER: 'PROFILE_UPDATE_TWITTER',
        PROFILE_UPDATE_FACEBOOK: 'PROFILE_UPDATE_FACEBOOK',
        RESOURCE_PROVIDE_FEEDBACK: 'RESOURCE_PROVIDE_FEEDBACK',
        RESOURCE_REQUEST_NEW: 'RESOURCE_REQUEST_NEW',

        // USER_COMPLETE_OFFERS: 'USER_COMPLETE_OFFERS',

        MARKETPLACE_POST: 'MARKETPLACE_POST',
        MARKETPLACE_POST_REPLY: 'MARKETPLACE_POST_REPLY',

        MESSAGE_SENT: 'MESSAGE_SENT',

        TAG_TYPE_OFFER:     'TYPE_OFFER',
        TAG_TYPE_REQUEST:   'TYPE_REQUEST',
        TAG_TYPE_LANGUAGE:  'TYPE_LANGUAGE',
        TAG_TYPE_SKILL:     'TYPE_SKILL',
        TAG_TYPE_BEAT:      'TYPE_BEAT',
        TAG_TYPE_FOCUS:     'TYPE_FOCUS',


        GAME_PROFILE:   'profile',
        GAME_COMMUNITY: 'community',
        GAME_AUDIENCE:  'audience',

        //Styles:
        ACCENT_COLOR: '#FC7108',
        PRIMARY_TEXT_COLOR: '#212121',
        DISABLED_COLOR: '#BDBDBD',
        // BACKGROUND_COLOR_GREY: '#FAFAFA',
        // BACKGROUND_COLOR_GREY: '#F1F1F1',
        BACKGROUND_COLOR_GREY: '#E7E9EC',
        BACKGROUND_COLOR_WHITE: '#FFF',

    };

    app.threadTypes = {
        P2P:                        'P2P',
        TASK_RESOURCE_FEEDBACK:     'TASK_RESOURCE_FEEDBACK',
        TASK_MARKETPLACE_FEEDBACK:  'TASK_MARKETPLACE_FEEDBACK',
        MARKETPLACE:                'MARKETPLACE',
        SUPPORT:                    'SUPPORT',
        GROUP:                      'GROUP',
        REFER:                      'REFER',
    };

    app.STATUS_RESPONSE = {
        //OK
        HTTP_CONTINUE:     100,
        SWITCH_PROTOCOLS:  101,

// Successful 2xx
        OK:                200,
        CREATED:           201,
        ACCEPTED:          202,
        NONAUTHORITATIVE:  203,
        NO_CONTENT:        204,
        RESET_CONTENT:     205,
        PARTIAL_CONTENT:   206,

// Client Error 4xx
        BAD_REQUEST:       400,
        UNAUTHORIZED:      401,
        PAYMENT_REQUIRED:  402,
        FORBIDDEN:         403,
        NOT_FOUND:         404,
        NOT_ALLOWED:       405,

// Server Error 5xx
        SERVER_ERROR:      500,

    };


    app.updateMainscrollHeight = function (){

        var mainDiv = document.querySelector('#mainScroll');
        if(!mainDiv) return;

        mainDiv.style.height = app.getMainContainerHeight() + 'px';
    };

    app.getMainContainerHeight = function () {

        var heightAux = app.getHeight();

        if(app.subheaderVisible){

            return heightAux - app.subheaderHeight - app.headerHeight;
        }else{
            return heightAux - app.headerHeight;
        }
    };

    /*
     * Obtener el alto del documento, compatible con todos los browsers
     */
    app.getDocumentHeight = function () {

        var myWidth = 0, myHeight = 0;
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            myHeight = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            myHeight = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            myHeight = document.body.clientHeight;
        }
        return myHeight;
    };

    app.isEmptyObject = function (obj) {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }

    app.isXsResolution = function () {
        return window.matchMedia("screen and (max-width: 48em)").matches;
    }

    app.countries = [
        {shortName: "AD", longName: "Andorra" },
        {shortName: "AE", longName: "United Arab Emirates" },
        {shortName: "AF", longName: "Afghanistan" },
        {shortName: "AG", longName: "Antigua and Barbuda" },
        {shortName: "AI", longName: "Anguilla" },
        {shortName: "AL", longName: "Albania" },
        {shortName: "AM", longName: "Armenia" },
        {shortName: "AO", longName: "Angola" },
        {shortName: "AQ", longName: "Antarctica" },
        {shortName: "AR", longName: "Argentina" },
        {shortName: "AS", longName: "American Samoa" },
        {shortName: "AT", longName: "Austria" },
        {shortName: "AU", longName: "Australia" },
        {shortName: "AW", longName: "Aruba" },
        {shortName: "AX", longName: "Åland Islands" },
        {shortName: "AZ", longName: "Azerbaijan" },
        {shortName: "BA", longName: "Bosnia and Herzegovina" },
        {shortName: "BB", longName: "Barbados" },
        {shortName: "BD", longName: "Bangladesh" },
        {shortName: "BE", longName: "Belgium" },
        {shortName: "BF", longName: "Burkina Faso" },
        {shortName: "BG", longName: "Bulgaria" },
        {shortName: "BH", longName: "Bahrain" },
        {shortName: "BI", longName: "Burundi" },
        {shortName: "BJ", longName: "Benin" },
        {shortName: "BL", longName: "Saint Barthélemy" },
        {shortName: "BM", longName: "Bermuda" },
        {shortName: "BN", longName: "Brunei Darussalam" },
        // {shortName: "BO", longName: "Bolivia (Plurinational State of)" },
        {shortName: "BO", longName: "Bolivia" },
        // {shortName: "BQ", longName: "Bonaire, Sint Eustatius and Saba" },
        {shortName: "BQ", longName: "Bonaire" },
        {shortName: "BR", longName: "Brazil" },
        {shortName: "BS", longName: "Bahamas" },
        {shortName: "BT", longName: "Bhutan" },
        {shortName: "BV", longName: "Bouvet Island" },
        {shortName: "BW", longName: "Botswana" },
        {shortName: "BY", longName: "Belarus" },
        {shortName: "BZ", longName: "Belize" },
        {shortName: "CA", longName: "Canada" },
        {shortName: "CC", longName: "Cocos (Keeling) Islands" },
        // {shortName: "CD", longName: "Congo (Democratic Republic of the)" },
        {shortName: "CD", longName: "Congo" },
        {shortName: "CF", longName: "Central African Republic" },
        {shortName: "CG", longName: "Congo" },
        {shortName: "CH", longName: "Switzerland" },
        {shortName: "CI", longName: "Côte d'Ivoire" },
        {shortName: "CK", longName: "Cook Islands" },
        {shortName: "CL", longName: "Chile" },
        {shortName: "CM", longName: "Cameroon" },
        {shortName: "CN", longName: "China" },
        {shortName: "CO", longName: "Colombia" },
        {shortName: "CR", longName: "Costa Rica" },
        {shortName: "CU", longName: "Cuba" },
        {shortName: "CV", longName: "Cabo Verde" },
        {shortName: "CW", longName: "Curaçao" },
        {shortName: "CX", longName: "Christmas Island" },
        {shortName: "CY", longName: "Cyprus" },
        {shortName: "CZ", longName: "Czech Republic" },
        {shortName: "DE", longName: "Germany" },
        {shortName: "DJ", longName: "Djibouti" },
        {shortName: "DK", longName: "Denmark" },
        {shortName: "DM", longName: "Dominica" },
        {shortName: "DO", longName: "Dominican Republic" },
        {shortName: "DZ", longName: "Algeria" },
        {shortName: "EC", longName: "Ecuador" },
        {shortName: "EE", longName: "Estonia" },
        {shortName: "EG", longName: "Egypt" },
        {shortName: "EH", longName: "Western Sahara" },
        {shortName: "ER", longName: "Eritrea" },
        {shortName: "ES", longName: "Spain" },
        {shortName: "ET", longName: "Ethiopia" },
        {shortName: "FI", longName: "Finland" },
        {shortName: "FJ", longName: "Fiji" },
        // {shortName: "FK", longName: "Falkland Islands (Malvinas)" },
        {shortName: "FK", longName: "Islas Malvinas" },
        // {shortName: "FM", longName: "Micronesia (Federated States of)" },
        {shortName: "FM", longName: "Micronesia" },
        {shortName: "FO", longName: "Faroe Islands" },
        {shortName: "FR", longName: "France" },
        {shortName: "GA", longName: "Gabon" },
        {shortName: "GB", longName: "United Kingdom" },
        {shortName: "GD", longName: "Grenada" },
        {shortName: "GE", longName: "Georgia" },
        {shortName: "GF", longName: "French Guiana" },
        {shortName: "GG", longName: "Guernsey" },
        {shortName: "GH", longName: "Ghana" },
        {shortName: "GI", longName: "Gibraltar" },
        {shortName: "GL", longName: "Greenland" },
        {shortName: "GM", longName: "Gambia" },
        {shortName: "GN", longName: "Guinea" },
        {shortName: "GP", longName: "Guadeloupe" },
        {shortName: "GQ", longName: "Equatorial Guinea" },
        {shortName: "GR", longName: "Greece" },
        // {shortName: "GS", longName: "South Georgia and the South Sandwich Islands" },
        {shortName: "GS", longName: "South Georgia" },
        {shortName: "GT", longName: "Guatemala" },
        {shortName: "GU", longName: "Guam" },
        {shortName: "GW", longName: "Guinea-Bissau" },
        {shortName: "GY", longName: "Guyana" },
        {shortName: "HK", longName: "Hong Kong" },
        {shortName: "HM", longName: "Heard Island and McDonald Islands" },
        {shortName: "HN", longName: "Honduras" },
        {shortName: "HR", longName: "Croatia" },
        {shortName: "HT", longName: "Haiti" },
        {shortName: "HU", longName: "Hungary" },
        {shortName: "ID", longName: "Indonesia" },
        {shortName: "IE", longName: "Ireland" },
        {shortName: "IL", longName: "Israel" },
        {shortName: "IM", longName: "Isle of Man" },
        {shortName: "IN", longName: "India" },
        {shortName: "IO", longName: "British Indian Ocean Territory" },
        {shortName: "IQ", longName: "Iraq" },
        {shortName: "IR", longName: "Iran (Islamic Republic of)" },
        {shortName: "IS", longName: "Iceland" },
        {shortName: "IT", longName: "Italy" },
        {shortName: "JE", longName: "Jersey" },
        {shortName: "JM", longName: "Jamaica" },
        {shortName: "JO", longName: "Jordan" },
        {shortName: "JP", longName: "Japan" },
        {shortName: "KE", longName: "Kenya" },
        {shortName: "KG", longName: "Kyrgyzstan" },
        {shortName: "KH", longName: "Cambodia" },
        {shortName: "KI", longName: "Kiribati" },
        {shortName: "KM", longName: "Comoros" },
        {shortName: "KN", longName: "Saint Kitts and Nevis" },
        {shortName: "KP", longName: "Korea (Democratic People's Republic of)" },
        {shortName: "KR", longName: "Korea (Republic of)" },
        {shortName: "KW", longName: "Kuwait" },
        {shortName: "KY", longName: "Cayman Islands" },
        {shortName: "KZ", longName: "Kazakhstan" },
        {shortName: "LA", longName: "Lao People's Democratic Republic" },
        {shortName: "LB", longName: "Lebanon" },
        {shortName: "LC", longName: "Saint Lucia" },
        {shortName: "LI", longName: "Liechtenstein" },
        {shortName: "LK", longName: "Sri Lanka" },
        {shortName: "LR", longName: "Liberia" },
        {shortName: "LS", longName: "Lesotho" },
        {shortName: "LT", longName: "Lithuania" },
        {shortName: "LU", longName: "Luxembourg" },
        {shortName: "LV", longName: "Latvia" },
        {shortName: "LY", longName: "Libya" },
        {shortName: "MA", longName: "Morocco" },
        {shortName: "MC", longName: "Monaco" },
        {shortName: "MD", longName: "Moldova (Republic of)" },
        {shortName: "ME", longName: "Montenegro" },
        {shortName: "MF", longName: "Saint Martin (French part)" },
        {shortName: "MG", longName: "Madagascar" },
        {shortName: "MH", longName: "Marshall Islands" },
        {shortName: "MK", longName: "Macedonia (the former Yugoslav Republic of)" },
        {shortName: "ML", longName: "Mali" },
        {shortName: "MM", longName: "Myanmar" },
        {shortName: "MN", longName: "Mongolia" },
        {shortName: "MO", longName: "Macao" },
        {shortName: "MP", longName: "Northern Mariana Islands" },
        {shortName: "MQ", longName: "Martinique" },
        {shortName: "MR", longName: "Mauritania" },
        {shortName: "MS", longName: "Montserrat" },
        {shortName: "MT", longName: "Malta" },
        {shortName: "MU", longName: "Mauritius" },
        {shortName: "MV", longName: "Maldives" },
        {shortName: "MW", longName: "Malawi" },
        {shortName: "MX", longName: "Mexico" },
        {shortName: "MY", longName: "Malaysia" },
        {shortName: "MZ", longName: "Mozambique" },
        {shortName: "NA", longName: "Namibia" },
        {shortName: "NC", longName: "New Caledonia" },
        {shortName: "NE", longName: "Niger" },
        {shortName: "NF", longName: "Norfolk Island" },
        {shortName: "NG", longName: "Nigeria" },
        {shortName: "NI", longName: "Nicaragua" },
        {shortName: "NL", longName: "Netherlands" },
        {shortName: "NO", longName: "Norway" },
        {shortName: "NP", longName: "Nepal" },
        {shortName: "NR", longName: "Nauru" },
        {shortName: "NU", longName: "Niue" },
        {shortName: "NZ", longName: "New Zealand" },
        {shortName: "OM", longName: "Oman" },
        {shortName: "PA", longName: "Panama" },
        {shortName: "PE", longName: "Peru" },
        {shortName: "PF", longName: "French Polynesia" },
        {shortName: "PG", longName: "Papua New Guinea" },
        {shortName: "PH", longName: "Philippines" },
        {shortName: "PK", longName: "Pakistan" },
        {shortName: "PL", longName: "Poland" },
        {shortName: "PM", longName: "Saint Pierre and Miquelon" },
        {shortName: "PN", longName: "Pitcairn" },
        {shortName: "PR", longName: "Puerto Rico" },
        {shortName: "PS", longName: "Palestine, State of" },
        {shortName: "PT", longName: "Portugal" },
        {shortName: "PW", longName: "Palau" },
        {shortName: "PY", longName: "Paraguay" },
        {shortName: "QA", longName: "Qatar" },
        {shortName: "RE", longName: "Réunion" },
        {shortName: "RO", longName: "Romania" },
        {shortName: "RS", longName: "Serbia" },
        {shortName: "RU", longName: "Russian Federation" },
        {shortName: "RW", longName: "Rwanda" },
        {shortName: "SA", longName: "Saudi Arabia" },
        {shortName: "SB", longName: "Solomon Islands" },
        {shortName: "SC", longName: "Seychelles" },
        {shortName: "SD", longName: "Sudan" },
        {shortName: "SE", longName: "Sweden" },
        {shortName: "SG", longName: "Singapore" },
        {shortName: "SH", longName: "Saint Helena, Ascension and Tristan da Cunha" },
        {shortName: "SI", longName: "Slovenia" },
        {shortName: "SJ", longName: "Svalbard and Jan Mayen" },
        {shortName: "SK", longName: "Slovakia" },
        {shortName: "SL", longName: "Sierra Leone" },
        {shortName: "SM", longName: "San Marino" },
        {shortName: "SN", longName: "Senegal" },
        {shortName: "SO", longName: "Somalia" },
        {shortName: "SR", longName: "Suriname" },
        {shortName: "SS", longName: "South Sudan" },
        {shortName: "ST", longName: "Sao Tome and Principe" },
        {shortName: "SV", longName: "El Salvador" },
        {shortName: "SX", longName: "Sint Maarten (Dutch part)" },
        {shortName: "SY", longName: "Syrian Arab Republic" },
        {shortName: "SZ", longName: "Swaziland" },
        {shortName: "TC", longName: "Turks and Caicos Islands" },
        {shortName: "TD", longName: "Chad" },
        {shortName: "TF", longName: "French Southern Territories" },
        {shortName: "TG", longName: "Togo" },
        {shortName: "TH", longName: "Thailand" },
        {shortName: "TJ", longName: "Tajikistan" },
        {shortName: "TK", longName: "Tokelau" },
        {shortName: "TL", longName: "Timor-Leste" },
        {shortName: "TM", longName: "Turkmenistan" },
        {shortName: "TN", longName: "Tunisia" },
        {shortName: "TO", longName: "Tonga" },
        {shortName: "TR", longName: "Turkey" },
        {shortName: "TT", longName: "Trinidad and Tobago" },
        {shortName: "TV", longName: "Tuvalu" },
        {shortName: "TW", longName: "Taiwan, Province of China[a]" },
        {shortName: "TZ", longName: "Tanzania, United Republic of" },
        {shortName: "UA", longName: "Ukraine" },
        {shortName: "UG", longName: "Uganda" },
        // {shortName: "UM", longName: "United States Minor Outlying Islands" },
        {shortName: "US", longName: "United States of America" },
        {shortName: "UY", longName: "Uruguay" },
        {shortName: "UZ", longName: "Uzbekistan" },
        {shortName: "VA", longName: "Holy See" },
        {shortName: "VC", longName: "Saint Vincent and the Grenadines" },
        {shortName: "VE", longName: "Venezuela" },
        {shortName: "VG", longName: "Virgin Islands (British)" },
        {shortName: "VI", longName: "Virgin Islands (U.S.)" },
        {shortName: "VN", longName: "Viet Nam" },
        {shortName: "VU", longName: "Vanuatu" },
        {shortName: "WF", longName: "Wallis and Futuna" },
        {shortName: "WS", longName: "Samoa" },
        {shortName: "YE", longName: "Yemen" },
        {shortName: "YT", longName: "Mayotte" },
        {shortName: "ZA", longName: "South Africa" },
        {shortName: "ZM", longName: "Zambia" },
        {shortName: "ZW", longName: "Zimbabwe" },
    ];


    app.getCountryByShortname = function(shortName){
        for (var i = 0; i < app.countries.length; i++) {
            if (app.countries[i].shortName == shortName) {
                return app.countries[i].longName;
            }
        }
        return '';
    }

    app.getCountriesComboList = function(){

        if(this.countryList) return this.countryList;

        var countryList = [];
        app.countries.forEach(function(country) {
            countryList.push({value:country.shortName, label: country.longName});
        });

        countryList.sort(function(a, b){
            if(a.label < b.label) return -1;
            if(a.label > b.label) return 1;
            return 0;
        });

        this.countryList = countryList;
        return countryList;
    }

})(document);
